<?php
declare(strict_types=1);

namespace App\Tests\Functional\Users\Infrastructure\Repository;

use App\Tests\Tools\FixtureTools;
use App\Users\Domain\Factory\UserFactory;
use App\Users\Domain\Repository\UserRepositoryInterface;
use Faker\Factory;
use Faker\Generator;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRepositoryTest extends WebTestCase
{
    use FixtureTools;

    private UserRepositoryInterface $repository;
    private UserFactory $userFactory;
    private Generator $faker;
    private AbstractDatabaseTool $databaseTool;

    public function setUp(): void
    {
        parent::setUp();

        $this->repository = static::getContainer()->get(UserRepositoryInterface::class);
        $this->userFactory = static::getContainer()->get(UserFactory::class);
        $this->faker = Factory::create();

        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();

    }

    public function test_user_added_successfully(): void
    {
        $email = $this->faker->email();
        $password = $this->faker->password();

        $user = $this->userFactory->create($email, $password);

        // act
        $this->repository->add($user);

        // assert
        $existingUser = $this->repository->findByUlid($user->getUlId());
        $this->assertEquals($user->getUlid(), $existingUser->getUlid());
    }

    public function test_user_found_successfully(): void
    {
        // arrange
        $user = $this->loadUserFixture();

        // act
        $existingUser = $this->repository->findByUlid($user->getUlid());

        // assert
        $this->assertEquals($user->getUlid(), $existingUser->getUlid());
    }
}
